"use strict";

let baseUrl = 'http://localhost:8001/';

async function getAllSessions() {
    let data = await fetch(baseUrl + '?q=sessions', { method: 'GET', })
        .then(response => response.json())
        .then(data => console.log(data));
    return data;
}

async function getSessionById(session_id) {
    let data = await fetch(baseUrl + '?q=chatmessage&session_id=' + session_id, { method: 'GET', })
        .then(response => response.json())
        .then(data => console.log(data));
    return data;
}

async function getNewSessionId(firstname, lastname, email) {
    let data = await fetch(baseUrl + '?q=sessions&firstname=' + firstname + 'lastname=' + lastname + 'email=' + email, { method: 'GET', })
        .then(response => response.json())
        .then(data => console.log(data));
    return data;
}

async function postNewSession(firstname, lastname, email) {
    await fetch(baseUrl + '?q=sessions&firstname=' + firstname + 'lastname=' + lastname + 'email=' + email, { method: 'POST', })
        .then(response => response.json())
        .then(data => console.log(data));
}

async function postMessageUser(session_id, message) {
    await fetch(baseUrl + '?q=chatmessage&session_id=' + session_id + 'message=' + message, { method: 'POST', })
        .then(response => response.json())
        .then(data => console.log(data));
}

async function postMessageModerator(session_id, message, moderator_id) {
    await fetch(baseUrl + '?q=chatmessage&session_id=' + session_id + 'message=' + message + 'moderator_id=' + moderator_id, { method: 'POST', })
        .then(response => response.json())
        .then(data => console.log(data));
}