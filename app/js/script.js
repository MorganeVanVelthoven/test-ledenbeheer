"use strict";

document.addEventListener("DOMContentLoaded", init);

let moderator_id = 1;

let chat_head = document.querySelector("#openchat-btn");
let chatroom = document.querySelector("#chatroom");
let chat_header = document.querySelector("#chat-header");

document.querySelector("form").addEventListener("submit", onFormSubmit);
document.querySelector("#btnSendMessage").addEventListener("click", sendMessage);

function init() {
    chat_head.addEventListener("click", openChatroom);
    chat_header.addEventListener("click", closeChatroom);
}

function openChatroom() {
    chat_head.style.display = "none";
    chatroom.style.display = "block";
    checkForSession();
}

function closeChatroom() {
    chat_head.style.display = "block";
    chatroom.style.display = "none";
}

function checkForSession() {
    if (localStorage.getItem("session_id") == null) {
        alert("A new session needs to be started. Enter your firstname, lastname and email adress to be able to send messages.");
        newSessionForm.style.display = "block";
    }
}

function onFormSubmit(e) {
    e.preventDefault();
    let firstname = document.getElementById("firstname").value;
    let lastname = document.getElementById("lastname").value;
    let email = document.getElementById("email").value;

    postNewSession(firstname, lastname, email)

    document.querySelector("form").reset();
    newSessionForm.style.display = "none";

    let session_id = getNewSessionId(firstname, lastname, email);
    localStorage.setItem("session_id", session_id);
}

function sendMessage() {
    let message = document.querySelector("#txtMessage");
    if (message.value === "") return;

    let session_id = localStorage.getItem("session_id");
    postMessageUser(session_id, message);
    message.value = "";

    answerMessage();
}

async function answerMessage() {
    let responses = await fetch('./responses.json')
        .then(response => response.json());
    let response_id = Math.floor((Math.random() * 6) + 1);

    let message = responses[response_id];

    postMessageModerator(session_id, message, moderator_id);
}