<?php
require_once('./db.php');

if (!isset($_GET['q'])) die('No specific API passed');

// List all chat sessions
if ($_GET['q'] === 'sessions' && !isset($_GET['id'])  && !isset($_GET['firstname'])) {
    $result = $conn->query('SELECT * FROM chat_sessions');

    if ($result) {
        $sessions = [];
        while($data = $result->fetch_assoc()) {
            $sessions[] = $data;
        }
        exit(json_encode($sessions));
    } else {
        print_r(mysqli_error($conn));
    }
}

// List all chat messages of a session
if ($_GET['q'] === 'sessions' && isset($_GET['id'])) {
    $id = $_GET['id'];
    $result = $conn->query('SELECT * FROM chat_message WHERE chat_session_id = ' . $id);

    if ($result) {
        $messages = [];
        while($data = $result->fetch_assoc()) {
            $messages[] = $data;
        }
        exit(json_encode($messages));
    } else {
        print_r(mysqli_error($conn));
    }
}

// Get the id of a session
if ($_GET['q'] === 'sessions' && isset($_GET['firstname']) && isset($_GET['lastname']) && isset($_GET['email'])) {
    $firstname = $_GET['firstname'];
    $lastname = $_GET['lastname'];
    $email = $_GET['email'];
    $sql = "SELECT * FROM chat_sessions WHERE firstname='" . $firstname . "' AND lastname='" . $lastname . "' AND email='" . $email . "'";
    $result = $conn->query($sql);

    if ($result) {
        $id = "";
        while($data = $result->fetch_assoc()) {
            $id = $data["id"];
        }
        exit(json_encode($id));
    } else {
        print_r(mysqli_error($conn));
    }
}

// Post a new session to the database
if ($_GET['q'] === 'sessions' && isset($_GET['firstname']) && isset($_GET['lastname']) && isset($_GET['email'])) {
    $firstname = $_GET['firstname'];
    $lastname = $_GET['lastname'];
    $email = $_GET['email'];
    $created_at = new DateTime();
    $created_at = $created_at->format('Y-m-d H:i:s');

    $sql = "INSERT INTO chat_sessions (firstname, lastname, email, created_at) VALUES ('" . $firstname . "','" . $lastname . "','" . $email . "','" . $created_at . "')";
    $result = $conn->query($sql);
    if ($result) {
        print_r("Request succesfully completed");
    } else {
        print_r(mysqli_error($conn));
    }
}

// Post message to database
// as moderator
if ($_GET['q'] === 'chatmessage' && isset($_GET['session_id']) && isset($_GET['moderator_id']) && isset($_GET['message'])) {
    $session_id = $_GET['session_id'];
    $moderator_id = $_GET['moderator_id'];
    $message = $_GET['message'];
    $created_at = new DateTime();
    $created_at = $created_at->format('Y-m-d H:i:s');

    $sql = "INSERT INTO chat_message (chat_session_id, moderator_id, message, created_at) VALUES ('" . $session_id . "','" . $moderator_id . "','" . $message . "','" . $created_at . "')";
    $result = $conn->query($sql);
    if ($result) {
        print_r("Request succesfully completed");
    } else {
        print_r(mysqli_error($conn));
    }
}

// Post message to database
// as user
if ($_GET['q'] === 'chatmessage' && isset($_GET['session_id']) && !isset($_GET['moderator_id']) && isset($_GET['message'])) {
    $session_id = $_GET['session_id'];
    $message = $_GET['message'];
    $created_at = new DateTime();
    $created_at = $created_at->format('Y-m-d H:i:s');

    $sql = "INSERT INTO chat_message (chat_session_id, message, created_at) VALUES ('" . $session_id . "', '" . $message . "', '" . $created_at . "')";
    $result = $conn->query($sql);
    if ($result) {
        print_r("Request succesfully completed");
    } else {
        print_r(mysqli_error($conn));
    }
}
